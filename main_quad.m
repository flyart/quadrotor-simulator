%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% QUADROTOR SIMULATOR - MAIN                                              %
% Authors:  Mattia Giurato (mattia.giurato@polimi.it)                     %
%           Paolo Gattazzo (paolo.gattazzo@polimi.it)                     %
% Date: 13/12/2017                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clearvars;
close all;
addpath('common','common/simulator-toolbox','common/simulator-toolbox/attitude_library','common/simulator-toolbox/trajectory_library');
clc;

%% Simulation setup
init.simulation_time = 100;                                                 %[s] min = 65s
init.fps = 25;                                                              %Frame Per Seconds video

%Initial condition
init.pos_ned = [0, 0, -0.065]';                                             %Initial position
init.vel_body = [0, 0, 0]';                                                 %Initial velocity
init.ang_rate_body = [0, 0, 0]';                                            %Initial angular rates
init.attitude = eulerToQuat([0, 0, deg2rad(45)]');                          %Initial attitude - quaternion

%Trajectory properties
traj.altitude = -1.0;                                                       %Altitude the drone reach after the take-off to perform any trajectory
traj.omega = 0.4;                                                           %Speed of execution of traj.                                                                        
traj.scale = 3;                                                             %Scaling factor of trajectories
traj.pitch_deg = 0;                                                         %Pitch for paol1 trajectory
traj.roll = 0;                                                              %Amplitude of roll sinusoidal oscillation
traj.yaw_following = 1;                                                     %1 to have a varying yaw. 0 for null yaw.
traj.speed = [1, 0, 0]';                                                    %speed for line trajectory
traj.amp = 0.1;                                                             %Amplitude for vertical oscillation traj.12
traj.ff = 1;                                                                %Vertical oscillation frequency (rad/s)                               
traj.theta_f = 2 * pi;                                                      %Angle at which ends the acceleration phase in traj.13

%Final condition
traj.pos_ned_fin = [0, 0, traj.altitude]';                                  %Final position
traj.attitude_fin = eulerToQuat([0, 0, 0]');                                %Final attitude - quaternion

%Choose the trajectory
traj.traj_n = 2;	%1-infinity shape trajetory
                    %2-eight shape trajectory
                    %3-circular trajectory
                    %4-polynomial trajectory
                    %5-position step
                    %6-infinity bad trajectory
                    %7-circular bad trajectory
                    %8-paol1 trajectory
                    %9-spiral trajectory
                    %10-follow a line
                    %11-from point A to point B
                    %12-vertical oscillating
                    %13-Circular accelerating

%Deltas for bad trajectories
traj.delta_pos_n = 0;        
traj.delta_pos_e = 0;                        

%% Launch SIMULATOR
sim simulator_quad

%% Parse results
parse_results

%% Plot
plot_quad

%% Video play/record
%Uncomment the selected choice.
video = 'novideo';
% video = 'play';
% video = 'playAndRecord';

videoName = strcat([datestr(now, 'yyyymmdd_hhMM_'),'quad']);
video_quad

%% Delete temporary files
if exist('slprj','dir')
    rmdir('slprj', 's')                                                    
end

%% END OF CODE