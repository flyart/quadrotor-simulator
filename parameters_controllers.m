%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% QUADROTOR PARAMETERS - CONTROLLERS                                      %
% Authors:  Mattia Giurato (mattia.giurato@polimi.it)                     %
%           Paolo Gattazzo (paolo.gattazzo@polimi.it)                     %
% Date: 15/12/2017                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% FCU characteristics
ctr.sample_frequency = 250;                                                 %[Hz]

ctr.sample_time = 1/ctr.sample_frequency;                                   %[s]

%% Ground effect compensation
ctr.ge_compensation = 0;                                                    %0 no compensation, 1 compensation, 2 motor by motor compensation

%% Mixer matrix - X configuration
b = drone.arm_length;

ctr.mixer_cross = [ -1/4, -2^(1/2)/(4*b),  2^(1/2)/(4*b),  1/(4*drone.sigma) ;
                    -1/4,  2^(1/2)/(4*b), -2^(1/2)/(4*b),  1/(4*drone.sigma) ;
                    -1/4,  2^(1/2)/(4*b),  2^(1/2)/(4*b), -1/(4*drone.sigma) ;
                    -1/4, -2^(1/2)/(4*b), -2^(1/2)/(4*b), -1/(4*drone.sigma)];
clear b

%% Saturations
sat.MIN_THROTTLE = 0;
sat.MAX_THROTTLE = 100;
sat.max_thrust = 4 * (drone.motor_thrust_vs_throttle(1) * 100 + drone.motor_thrust_vs_throttle(2)*100^2);  %[N]
sat.max_torqueLM = (sqrt(2)/2) * drone.arm_length * sat.max_thrust / 2;	%[Nm]
sat.max_torqueN = 2 * drone.sigma * sat.max_thrust;            %[Nm]                                                                            
            
%% Angular-rate regulators
%p
ctr.att.KF_P = 0     * sat.max_torqueLM;
ctr.att.KP_P = 0.15  * sat.max_torqueLM;
ctr.att.KI_P = 0.05  * sat.max_torqueLM;
ctr.att.KD_P = 0.003 * sat.max_torqueLM;
ctr.att.L_MAX = 1.5;
ctr.att.L_MIN = -1.5;

%q
ctr.att.KF_Q = 0     * sat.max_torqueLM;
ctr.att.KP_Q = 0.15  * sat.max_torqueLM;
ctr.att.KI_Q = 0.05  * sat.max_torqueLM;
ctr.att.KD_Q = 0.003 * sat.max_torqueLM;
ctr.att.M_MAX = 1.5;
ctr.att.M_MIN = -1.5;

%r
ctr.att.KF_R = 0   * sat.max_torqueN;
ctr.att.KP_R = 0.2 * sat.max_torqueN;
ctr.att.KI_R = 0.1 * sat.max_torqueN;
ctr.att.KD_R = 0   * sat.max_torqueN;
ctr.att.N_MAX = 1.5;
ctr.att.N_MIN = -1.5;

ctr.att.N_filter_rate = 100;

%% Attitude regulators
%Roll
ctr.att.KP_ROLL = 6.5;
ctr.att.P_MAX = 10;	% 10 [rad/s] ~ 600 deg/s
ctr.att.P_MIN = -10;

%Pitch
ctr.att.KP_PITCH = 6.5;
ctr.att.Q_MAX = 10; % 10 [rad/s] ~ 600 deg/s
ctr.att.Q_MIN = -10;

%Yaw
ctr.att.KP_YAW = 2.8;
ctr.att.R_MAX = 10; % 10 [rad/s] ~ 600 deg/s
ctr.att.R_MIN = -10;

%% Linear-speed regulators
%N_dot
ctr.pos.KF_NORTH_DOT = 0    * sat.max_thrust;
ctr.pos.KP_NORTH_DOT = 0.09 * sat.max_thrust;
ctr.pos.KI_NORTH_DOT = 0.02 * sat.max_thrust;
ctr.pos.KD_NORTH_DOT = 0.01 * sat.max_thrust;
ctr.pos.F_NORTH_MAX = drone.mass * env.gravity / 2;
ctr.pos.F_NORTH_MIN = -ctr.pos.F_NORTH_MAX;

%E_dot
ctr.pos.KF_EAST_DOT = 0    * sat.max_thrust;
ctr.pos.KP_EAST_DOT = 0.09 * sat.max_thrust;
ctr.pos.KI_EAST_DOT = 0.02 * sat.max_thrust;
ctr.pos.KD_EAST_DOT = 0.01 * sat.max_thrust;
ctr.pos.F_EAST_MAX = ctr.pos.F_NORTH_MAX;
ctr.pos.F_EAST_MIN = ctr.pos.F_NORTH_MIN;

%D_dot
ctr.pos.KF_DOWN_DOT = 0    * sat.max_thrust;
ctr.pos.KP_DOWN_DOT = 0.2  * sat.max_thrust;
ctr.pos.KI_DOWN_DOT = 0.02 * sat.max_thrust;
ctr.pos.KD_DOWN_DOT = 0    * sat.max_thrust;
ctr.pos.F_DOWN_MAX = drone.mass * env.gravity / 2;
ctr.pos.F_DOWN_MIN = -ctr.pos.F_DOWN_MAX;

ctr.pos.N_filter_vel = 10;

%% Position regulator
%N
ctr.pos.KP_NORTH = 0.95;
ctr.pos.VEL_NORTH_MAX = 10; 
ctr.pos.VEL_NORTH_MIN = -10;

%E
ctr.pos.KP_EAST = ctr.pos.KP_NORTH;
ctr.pos.VEL_EAST_MAX = ctr.pos.VEL_NORTH_MAX; 
ctr.pos.VEL_EAST_MIN = ctr.pos.VEL_NORTH_MIN;

%D
ctr.pos.KP_DOWN = 1;
ctr.pos.VEL_DOWN_MAX = 1; 
ctr.pos.VEL_DOWN_MIN = -3;

%% END OF CODE