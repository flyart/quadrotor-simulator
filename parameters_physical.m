%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% QUADROTOR PARAMETERS - PHYSICAL                                         %
% Authors:  Mattia Giurato (mattia.giurato@polimi.it)                     %
%           Paolo Gattazzo (paolo.gattazzo@polimi.it)                     %
% Date: 23/04/2018                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Constants
%Environmental
env.gravity = 9.81;                                                         %[m*s^-2] Gravity acceleration
env.air_density = 1.225;                                                    %[kg*m^-3] Air density

%% Parameters definition
%Geometry informations
drone.arm_length = 0.55/2;                                                   %[m] Arm lenght

%Mass and structural informations
drone.mass = 1.510;                                                          %[kg] Body mass
drone.inertia_xx = 0.035;                                                    %[kg*m^2] Inertia around Xbody axes
drone.inertia_yy = 0.035;                                                    %[kg*m^2] Inertia around Ybody axes
drone.inertia_zz = 0.05;                                                     %[kg*m^2] Inertia around Zbody axes

drone.mass_matrix = drone.mass * eye(3);                                      %[Kg] Mass matrix
drone.mass_matrix_inv = (1/drone.mass) * eye(3);                              %[Kg^-1] Inverse of mass matrix
drone.inertia_tensor = diag([drone.inertia_xx drone.inertia_yy drone.inertia_zz]);  %[kg*m^2] Inertia tensor
drone.inertia_tensor_inv = drone.inertia_tensor \ eye(3);                     %[kg^-1*m^-2] Inverse of inertia tensor

%Propellers information
drone.propeller_diameter = 12 * 2.54 / 100;                                  %[m] Propeller diameter
drone.blades_number = 2;                                                     %[1] Number of blades
drone.propeller_chord = 0.0204;                                              %[m] Average chord length
drone.thrust_coefficient = 0.011859;                                         %[1] Thrust coefficent
drone.torque_coefficient = 0.00091322;                                       %[1] Torque coefficent
drone.motor_thrust_vs_throttle = [0.0281, 0.0011];                           %Thrust vs throttle: Y = b*X + a*X^2; motor_ome_vs_throttle = [b a]
drone.motor_tau = 0.05;                                                      %[s] Motor+Propeller time constant

drone.propeller_radius = drone.propeller_diameter/2;                          %[m] Propeller radius
drone.propeller_area = pi * drone.propeller_radius^2;                         %[m^2] Disk area
drone.blade_area = drone.blades_number * drone.propeller_radius * drone.propeller_chord;	%[m] Blade area
drone.propeller_solidity = drone.blade_area / drone.propeller_area;            %[1] Solid ratio
drone.Kt = drone.thrust_coefficient * env.air_density * drone.propeller_area * drone.propeller_radius^2;
drone.Kq = drone.torque_coefficient * env.air_density * drone.propeller_area * drone.propeller_radius^3;
drone.sigma = drone.Kq / drone.Kt;
drone.omega_hover = sqrt((drone.mass * env.gravity / drone.Kt) / 4);           %[rad/s]
drone.dM_du = 4 * sqrt(2) * drone.Kt * drone.arm_length * drone.omega_hover;    %[Nm*s] Control derivative

%Aerodynamic damping
drone.dL_dp = -0.046271;                                                     %[Nm*s] Stability derivative of the vehicle roll
drone.dM_dq = -0.046271;                                                     %[Nm*s] Stability derivative of the vehicle pitch
drone.dN_dr = -0.0185;                                                       %[Nm*s] Stability derivative of the vehicle yaw

drone.dLMN = [drone.dL_dp      0          0     ;
                  0     drone.dM_dq      0     ;
                  0          0     drone.dN_dr];
   
drone.Cd = -0.4778;

%Ground effect
drone.rho = 0.6438;

%% Map matrix - X configuration
b = drone.arm_length;
drone.map_cross = [             -1,             -1,            -1,             -1 ;
                    -(2^(1/2)*b)/2,  (2^(1/2)*b)/2, (2^(1/2)*b)/2, -(2^(1/2)*b)/2 ;
                     (2^(1/2)*b)/2, -(2^(1/2)*b)/2, (2^(1/2)*b)/2, -(2^(1/2)*b)/2 ;
                       drone.sigma,    drone.sigma,  -drone.sigma,   -drone.sigma];
                 
clear b

%% State estimators
noise.Enabler = 0;
% noise.Enabler = 1;

noise.pos_n_stand_dev = noise.Enabler * 0.0011;                            	%[m]
noise.pos_e_stand_dev = noise.Enabler * 0.0009;                             %[m]
noise.pos_d_stand_dev = noise.Enabler * 0.0013;                             %[m]

noise.vel_n_stand_dev = noise.Enabler * 0.01;                               %[m/s]
noise.vel_e_stand_dev = noise.Enabler * 0.01;                               %[m/s]
noise.vel_d_stand_dev = noise.Enabler * 0.01;                               %[m/s]

noise.attitude_stand_dev = noise.Enabler * deg2rad(0.0076);                 %[rad]
noise.ang_rate_stand_dev = noise.Enabler * deg2rad(0.01);                   %[rad/s]

%% Delays
delay.position_filter = 1;
delay.attitude_filter = 1;
delay.mixer = 1;

%% END OF CODE