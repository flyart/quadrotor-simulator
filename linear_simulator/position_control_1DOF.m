%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% QUADROTOR POSITION CONTROL TUNING                                       %
% Authors:  Mattia Giurato (mattia.giurato@polimi.it)                     %
%           Paolo Gattazzo (paolo.gattazzo@polimi.it)                     %
% Date: 23/04/2018                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clearvars;
close all;
clc;

%% Load attitude closed-loop model
load attitude_CL

%% Define linearized model
s = tf('s');

frequency_rate = 250;
sample_time = 1/frequency_rate;

Sum_dist = sumblk('Ndot_d = Ndot + d');

mass = 1.510;
gravity = 9.81;
Cd = 0.4778;

G_Ndot = mass * gravity / (mass * s + Cd);
G_Ndot_d = c2d(G_Ndot, sample_time);
G_Ndot_d.u = '\Theta'; G_Ndot_d.y = 'Ndot';

G_N = 1/s;
G_N_d = c2d(G_N, sample_time);
G_N_d.u = 'Ndot_d'; G_N_d.y = 'N';

%% Create inner loop
KF_NORTH_DOT = 0.5;
KP_NORTH_DOT = 5;
KI_NORTH_DOT = 0.3;
KD_NORTH_DOT = 0.5;
N_filter_vel = 10;

inner_PID = custom_PID( KF_NORTH_DOT, KP_NORTH_DOT, KI_NORTH_DOT, KD_NORTH_DOT, N_filter_vel, sample_time );
inner_PID.u = {'Ndot_0', 'Ndot_d'};
inner_PID.y = {'e_{Ndot}', 'F_0'};

scaler = tf(1, 45, sample_time);
scaler.u = {'F_0'};
scaler.y = {'\Theta_0'};

inner_CL = connect(inner_PID, scaler, attitude_CL('\Theta', '\Theta_0'), Sum_dist, G_Ndot_d, ...
    {'Ndot_0', 'd'},{'e_{Ndot}', '\Theta_0', 'Ndot_d'});

%% Create outer loop
KP_NORTH = 1;

P_o = tf(KP_NORTH);
P_o.u = 'e_N';
P_o.y = 'Ndot_0';
    
Sum_error = sumblk('e_N = N_0 - N');

position_CL = connect(P_o, Sum_error, inner_CL, G_N_d, ...
    {'N_0', 'Ndot_0', 'd'},{'e_N', 'e_{Ndot}', '\Theta_0', 'Ndot_d', 'N'});

%% Sensitivity functions
% S: N_0 -> e_N
S = position_CL('e_N', 'N_0');

% T: N_0 -> N
T = position_CL('N','N_0');

% K: N_0 -> \Theta_0
K = position_CL('\Theta_0','N_0');

f1 = figure('units','normalized','outerposition',[0 0 1 1]);
bodemag(S, 'r',  T, 'b', K, 'g',{1e-2,1e3})
title('Sensitivity functions with associated weight')
grid minor
legend('S', 'T', 'K')

%% Step
f2 = figure('units','normalized','outerposition',[0 0 1 1]);
subplot(211)
step(position_CL('N','N_0'))
subplot(212)
step(position_CL('\Theta','N_0'))

%% END OF CODE