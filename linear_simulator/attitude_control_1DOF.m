%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% QUADROTOR ATTITUDE CONTROL                                              %
% Authors:  Mattia Giurato (mattia.giurato@polimi.it)                     %
%           Paolo Gattazzo (paolo.gattazzo@polimi.it)                     %
% Date: 23/04/2018                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clearvars;
close all;
clc;

%% Load identified model
load pitch_model

%% Define linearized model
frequency_rate = 250;
sample_time = 1/frequency_rate;

s = tf('s');

Sum_dist = sumblk('q_d = q + d');

G_theta = 1/s;
G_theta.u = 'q_d'; G_theta.y = '\Theta';

%Convert model from continuous to discrete time
G_q_d = c2d(pitch_model, sample_time);
G_theta_d = c2d(G_theta, sample_time);

%% Create inner loop
KF_P = 0.045;
KP_P = 0.32;
KI_P = 0.194;
KD_P = 0.0242;
N_filter_rate = 100;

inner_PID = custom_PID( KF_P, KP_P, KI_P, KD_P, N_filter_rate, sample_time );
inner_PID.u = {'q_0', 'q_d'};
inner_PID.y = {'e_q', '\deltaM'};

inner_CL = connect(inner_PID, G_q_d, Sum_dist, ...
    {'q_0', 'd'},{'e_q', '\deltaM', 'q_d'});

%% Create outer loop
KP_ROLL = 2.34;

P_o = tf(KP_ROLL);
P_o.u = 'e_{\Theta}';
P_o.y = 'q_0';

Sum_error = sumblk('e_{\Theta} = \Theta_0 - \Theta');

attitude_CL = connect(P_o, Sum_error, inner_CL, G_theta_d, ...
    {'\Theta_0', 'q_0', 'd'},{'e_{\Theta}', 'e_q', '\deltaM', 'q_d', '\Theta'});

%% Sensitivity functions
% S: \Theta_0 -> e_{\Theta}
S = attitude_CL('e_{\Theta}', '\Theta_0');

% T: \Theta_0 -> \Theta
T = attitude_CL('\Theta','\Theta_0');

% K: \Theta_0 -> \deltaM
K = attitude_CL('\deltaM','\Theta_0');

f1 = figure('units','normalized','outerposition',[0 0 1 1]);
bodemag(S, 'r',  T, 'b', K, 'g', {1e-2,1e3})
title('Sensitivity functions')
grid minor
legend('S', 'T', 'K')

%% Step response
f2 = figure('units','normalized','outerposition',[0 0 1 1]);
subplot(211)
step(attitude_CL('\Theta','\Theta_0'))
subplot(212)
step(attitude_CL('\deltaM','\Theta_0'))

%% END OF CODE